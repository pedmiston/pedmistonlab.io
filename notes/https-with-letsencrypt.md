1. Install `certbot` with Homebrew.
2. Run `certbot certonly --manual -d MYDOMAIN.org`. In return, get a blob of data to put at a particular path on the site. **Leave this terminal window open.**
3. Create a new file at the specified path with the required data. Test it out locally, then deploy by pushing to GitLab.
4. Verify the data on the live server. Then, go back to the terminal window in Step 2, and continue.
5. Upload the certificate and the key to GitLab. Go to Settings -> Pages, remove the old CNAME, and add a new one with the same domain but a new TLS certificate. Paste the contents of `/etc/letsencrypt/live/MYDOMAIN.org/fullchain.pem` to the "Certificate (PEM)" and `/etc/letsencrypt/live/MYDOMAIN.org/privkey.pem` to the "Key (PEM)" field.
