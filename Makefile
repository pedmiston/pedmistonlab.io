curriculum-vitae.pdf: content/cv.md layouts/cv/single.html
	hugo && phantomjs bin/cv.js

static/team-structures.svg:
	dot -Tsvg -o $@ `Rscript -e "cat(crotchet::find_graphviz('team-structures', 'totems'))"`
