+++
categories = ["Others"]
date = "2017-05-23T22:48:39-05:00"
title = "Home"

+++

I'm a Psychology PhD at University of Wisconsin-Madison. My dissertation
is on **diachronic collaboration**: a form of teamwork based around
inheritance where one person works on a solution to a problem for a
while before passing it off to the next person to continue. I'm interested in how the effectiveness of diachronic teamwork compares to alternative ways of spending the same number of labor hours.

![](team-structures.svg)
