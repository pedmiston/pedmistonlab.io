+++
categories = ["Others"]
date = "2017-05-31"
title = "Curriculum vitae"
slug = "cv"
type = "cv"

+++

# [Pierce Edmiston](https://pedmiston.com)

> The science of change  

## Education

2013-
:   **PhD, Psychology**. University of Wisconsin-Madison.

2012-2013
:   **MS, Psychology**. University of Wisconsin-Madison.

2008-2012
:   **BA, Individualized (Cognitive Science)**. St. John's University, Collegeville, MN. Honors: _summa cum laude_, Phi Beta Kappa

## Leadership training and experience

9/1/2013-
:    Research assistant for Gary Lupyan's research lab.

3/6/2017
:    Bias and Diversity Training. University of Wisconsin-Madison.

START-END
:    Chair, Psychology Department Colloquium Series.

START-END
:    Manager, Athletic Training Room, College of St. Benedict.

## Journal publications

Edmiston, P., Perlman, M., & Lupyan, G. (submitted). The emergence of words from vocal imitations.

Edmiston, P., & Lupyan, G., (2017). Visual interference disrupts visual knowledge. _Journal of Memory and Language_, _92_, 281-292. [pdf][visual].

[visual]: http://sapir.psych.wisc.edu/papers/edmiston_lupyan_JML.pdf

Edmiston, P., & Lupyan, G., (2015). What makes words special? Words as unmotivated cues. _Cognition_, _143_, 93-100. [pdf][motivated], [press][npr].

[motivated]: http://sapir.psych.wisc.edu/papers/edmiston_lupyan_2015_motivated.pdf
[npr]: http://www.npr.org/sections/13.7/2015/07/14/422527144/the-magic-of-words-transcending-the-tyranny-of-the-specific

## Conference proceedings

Edmiston, P., Perlman, M., & Lupyan, G. (2016). The fidelity of iterated vocal imitation. In S. G. Roberts, C. Cuskley, L. McCrohon, L. Barceló-Coblijn, O. Feher, & T. Verhoef (Eds.), _The Evolution of Language: Proceedings of the 11th International Conference (EVOLANG11)_. New Orleans, LA. [abstract][abstract], [talk][talk], [press][press].

[abstract]: http://evolang.org/neworleans/papers/189.html
[talk]: http://sapir.psych.wisc.edu/evolang/fidelity.html
[press]: http://www.sciencemag.org/news/2016/03/buzz-thwack-how-sounds-become-words

Edmiston, P. & Lupyan, G. (2015). Visual interference disrupts visual and only visual knowledge. _Journal of Vision_, _15(12)_: 10. [abstract][vss].

[vss]: http://jov.arvojournals.org/article.aspx?articleid=2433048

Edmiston, P. & Lupyan, G. (2014). Words as unmotivated cues. In E. A. Cartmill, S. Roberts, H. Lyn, & H. Cornish (Eds.), _The Evolution of Language: Proceedings of the 10th International Conference (EVOLANG10)_. Vienna, Austria. [pdf][evolangx].

[evolangx]: http://sapir.psych.wisc.edu/papers/Edmiston_Lupyan_EvoLangX.pdf

Edmiston, P., & Lupyan, G. (2013). Verbal and nonverbal cues activate concepts differently, at different times. In M. Knauff, M. Pauen, N. Sebanz, & I. Wachsmuth (Eds.), _Proceedings of the 35th Annual Conference of the Cognitive Science Society_ (pp. 2243-2248). Austin, TX: Cognitive Science Society. [pdf][cogsci].

[cogsci]: http://csjarchive.cogsci.rpi.edu/Proceedings/2013/papers/0410/paper0410.pdf

## Manuscripts in preparation

Edmiston, P., Derex, M., & Lupyan, G. The role of inheritance in problem solving ability.

Edmiston, P., & Lupyan, G. Article quality as a selection pressure in the evolution of Wikipedia articles.
